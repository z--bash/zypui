# ZypUI

ZypUI provides useful and advanced Zypper/RPM cli commands in a convenient and easy to use text interface.

ZypUI is aimed at experienced/intermediate/advanced users of OpenSUSE (and other rpm-based distributions), who have at least basic knowledge of their Linux system, Zypper and repository management. Absolute beginners are probably overwhelmed by the choices ZypUI offers.

This project is directly inspired by the [`pacui`](https://github.com/excalibur1234/pacui/blob/master/README.md) that follows the KISS principle: The whole script is contained within one file, which consists of easy to read bash code with many helpful comments. ZypUI offers roughly the same amount of features as `pacui` which helps to speed up CLI based package management.


Table of Contents
-----------------

   * [User Interface](#user-interface)
   * [Installation](#installation)
      * [Execute without prior Installation](#execute-without-prior-installation)
      <!--* [OpenSUSE](#opensuse)-->
   * [Usage](#usage)
      <!-- * [Start ZypUI with UI](#start-zypui-with-ui)
      * [Start ZypUI without UI: Using Options](#start-zypui-without-ui-using-options)
      * [Start ZypUI without UI: Using Options and Package Names](#start-zypui-without-ui-using-options-and-package-names) -->
   * [Useful Tips and Recommended Settings](#useful-tips-and-recommended-settings)
      * [Alias](#alias)
      * [Search syntax](#search-syntax)
   <!-- * [Help](#help)
      * [Short ZypUI Help](#short-zypui-help)
      * [Detailed ZypUI Help](#detailed-zypui-help) -->


## User Interface
This is how `zypui` looks in terminal ( + optional colors ):
```

                            Zypper UI
 +----------------------------------------------------------+
 |  1  Update System            2  Maintain System          |
 |  3  Install Packages         4  Remove Packages and Deps |
 |----------------------------------------------------------|
 |  5  List Installed by Time   6  List Installed by Size   |
 |  7  List Package Files       8  Search Package Files     |
 +----------------------------------------------------------+
    Y  Distribution Upgrade     V  View Repositories
    D  Dependency Tree          E  Reverse Dependency Tree
 +-------------------------+
 |  9  Cleanup Old Kernels |    X  Index (2021-10-24 17:10)
 +-------------------------+
   Press number or letter                  ?  Help  0  Quit


```
Apart from awesome `fzf` only default system packages are used, thus `zypui` is blazing fast especially with offline package index.


## Installation

### Execute without prior Installation
For a minimal working version of ZypUI, please install its dependency: [fzf](https://github.com/junegunn/fzf) (and [openSUSE:Factory](https://build.opensuse.org/package/show/openSUSE%3AFactory/fzf)) using Zypper first (if possible). Then, the ZypUI file can be downloaded and run without prior installation:
```
  wget https://gitlab.com/z--bash/zypui/-/raw/main/zypui
  bash zypui
```
I find this feature of ZypUI invaluable for fixing systems. Here are two examples:

- A large number of updates broke (parts of) the GUI, e.g. xorg, window manager, or desktop environment. In this case, switching to a different tty (with CTRL + ALT + F2), installing ZypUI and using "Roll Back System" to roll back all the latest updates can fix the system (temporarily).

- A broken keyring makes it impossible to apply updates or install any packages. Executing ZypUI without prior installation and using "Install Packages (Advanced)" (which requires "fzf" only) to install packages from non-default target repository and is the fastest solution I know of.


## Usage

<!-- ### Start ZypUI with UI -->
After successful installation, type the following command into your terminal in order to start ZypUI with a simple UI:
```
  zypui
```


## Useful Tips and Recommended Settings

Along with ZypUI the following settings are recommended by the author:

### Alias
If you use ZypUI without the UI it is recommended to use an alias for ZypUI to reduce the amount of necessary typing. Do this by adding the following line to your ~/.bashrc file (if you use bash):
```
  alias zypui='bash ~/zypui'
```
This will set "zypui" as an alias to "zypui" within your terminal (after a restart of your shell or computer).

<!-- For example, you can now update your system using
```
  zypui u
```
-->

### Search syntax
ZypUI uses [fuzzy finder (fzf)](https://github.com/junegunn/fzf) to display lists of items (such as packages, package groups, logs, patchs, etc.) and by starting to type, you can easily search/filter those lists. Regular expressions can be used to improve the search results, e.g.:

| Search Term   | Description                       |
| ------------- | --------------------------------- |
|  `"sbtrkt"`   | Items that match `sbtrkt`         |
|  `"^music"`   | Items that start with `music`     |
|  `"git$"`     | Items that end with `git`         |
|  `"'wild"`    | Items that include `wild`         |
|  `"!fire"`    | Items that do not include `fire`  |
|  `"!-git$"`   | Items that do not end with `-git` |

A single bar character term acts as an OR operator. For example, the following query matches entries that start with `core` or end with `go`, `rb`, or `py`.
```
  "^core|go$|rb$|py$"
```

# Important remarks

While basic index (first 5-10 seconds) is fast, full info queries for remote packages is extremely slow because they are not batch yet and are simply done one by one which is extremely ineffective.

You should Ctrl+C out of index process unless you want to wait the eternity once you see the counter appear.

For now package installation candidate is chosen according to repo priority first choice.

---

# TODO

* Query remote package info in a batch manner.
* Package index descriptors should be named after rpm names (that include version and architecture) instead of package names for indexing to be more effective.
* Make it possible to select different version (repo) for installation candidate.

<!--

### OpenSUSE
In Manjaro, you can simply install the stable version of ZypUI from the Manjaro repositories:
```
  sudo zypper in zypui
```

--

### Start ZypUI without UI: Using Options
Using ZypUI without its UI requires less keystrokes and can therefore be much quicker. An overview of all ZypUI options is displayed with `zypui h`.

For example, you want to display the **R**everse dependency **T**ree of a package. Please note the marked letters "R" and "T" in ZypUI's corresponding UI option.
ZypUI does not care, whether you use upper or lower case letters as options or whether you use no, one or two dashes in front. Therefore, the following four commands are equivalent:
- `zypui RT`
- `zypui rt`
- `zypui -rt`
- `zypui --rt`

This principle can be used with all of ZypUI's options. Here is another random example (of ZypUI's hidden "List Packages by Size" option):
- `zypui LS`
- `zypui -LS`
- `zypui --LS`
- `zypui ls`
- `zypui -ls`
- `zypui --ls`

### Start ZypUI without UI: Using Options and Package Names
You can also use package names in addition to options. For example, you want to install the package "steam". Then, you can use a command like
```
  zypui i steam
```
Instead of a list of all available packages, a much shorter already filtered list is displayed. Simply select the "steam" package you want to install and press ENTER in order to install it.

If an argument contains special characters, it has to be passed on as string. This can be achieved differently depending on the used shell. For example when using regular expressions in zsh in order to search package file names starting with string "kernel-":
```
  zypui s '^kernel-'
```

### Start ZypUI without UI: Passing Arguments
For advanced use (e.g. in scripting or an alias), ZypUI can have a "flag" argument, which gets passed directly to package manager.
Examples:
- ` zypui -r 0ad --flag="--noconfirm"`
- ` zypui u flag --noconfirm`
- ` zypui --FLAG --asdeps --i --bash`
- ` zypui b --flag=--noconfirm`

--

## Help

### Short ZypUI Help
For short help, e.g. when using ZypUI without UI, use one of the following commands:
- `zypui h`
- `zypui -h`

### Detailed ZypUI Help
Choose the "Help" option within ZypUI's UI by pressing "H" or "h".
`zypui --help` from the terminal will call ZypUI's detailed help page, too.

This help page explains some general stuff such as how to navigate ZypUI. It also explains every ZypUI option in detail. If you want to look up which commands ZypUI uses under the hood and understand them in order to use ZypUI correctly, this is the right place for you!

-->
